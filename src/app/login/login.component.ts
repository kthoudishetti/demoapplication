import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailid;
  formdata;
  emailidPrime;
  passwordPrime;

  formdataValidate;
  emailValidate;

//Model Driven form validation with email and password
  ngOnInit() {

    this.formdataValidate = new FormGroup({
      emailValidate: new FormControl("", Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])),
      passwordValidate: new FormControl("", this.passwordvalidation)
    });
  }

  passwordvalidation(formcontrol) {
    if (formcontrol.value.length < 5) {
      return { "passwordValidate": true };
    }
  }

  onClickSubmitValidate(data) {
    this.emailValidate = data.emailValidate;
  }

  onSubmitModelDriven(data) {
    alert("Entered Email id : " + data.emailidPrime);
    alert("please enter another set of email and password");
    this.emailidPrime = data.emailidPrime;
  }

  onClickSubmit(data) {
    alert("Entered Email id : " + data.emailid);
    alert("please enter another set of email and password");
  }

  // this.formdata = new FormGroup({
  //   emailidPrime: new FormControl("kumarDev@gmail.com"),
  //   passwordPrime: new FormControl("dummypassword")
  // });

}
