import { Component, OnInit } from '@angular/core';
import { MyServiceService } from '../my-service.service'
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  public personaData = [];
  public photoAlbumDetails = [];
  public userDetails = [];
  constructor(private myService: MyServiceService) { }

  ngOnInit() {
    this.myService.getData().subscribe((data) => {
      this.personaData = Array.from(Object.keys(data), k => data[k]);
      //console.log(this.personaData);
    });

    this.myService.getPhotosData().subscribe((data) => {
      this.photoAlbumDetails = Array.from(Object.keys(data), k => data[k]);
      console.log(this.photoAlbumDetails);
    });

    this.myService.getUsersData().subscribe((data) => {
      this.userDetails = Array.from(Object.keys(data), k => data[k]);
      console.log(this.userDetails);
    });

  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

}
