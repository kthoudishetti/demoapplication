import { Component, OnInit } from '@angular/core';
import {MyServiceService} from '../my-service.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
myComponentProperty;
myDateProperty;
  constructor(private myservice: MyServiceService) {}

  ngOnInit(){
    this.myComponentProperty = this.myservice.serviceProperty;
    console.log(this.myComponentProperty);
    this.myservice.serviceProperty = "Service Property updated from Offers Component";
    this.myComponentProperty = this.myservice.serviceProperty;
    console.log(this.myComponentProperty);
    this.myDateProperty = this.myservice.showTodayDate();
  }

}
