import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { RouterModule } from '@angular/router';
import { TopbarComponent } from './topbar/topbar.component';
import { ServicesComponent } from './services/services.component';
import { OffersComponent } from './offers/offers.component';
import { LoginComponent } from './login/login.component';
import { TodoComponent } from './todo/todo.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MyServiceService } from './my-service.service';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path : 'home', component: HomeComponent},
      { path : 'services', component: ServicesComponent},
      { path : 'offers', component: OffersComponent},
      { path : 'contactus', component : ContactUsComponent},
      { path : 'login', component: LoginComponent},
      { path : 'todo', component: TodoComponent},
    ]),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollDispatchModule,
    DragDropModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    TopbarComponent,
    HomeComponent,
    ContactUsComponent,
    ServicesComponent,
    OffersComponent,
    LoginComponent,
    TodoComponent
  ],
  providers: [MyServiceService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
