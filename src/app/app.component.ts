import { Component } from '@angular/core';
import {MyServiceService} from './my-service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todaydate;
  myProperty;
  constructor(private myservice: MyServiceService) {}
  ngOnInit() { 
     this.todaydate = this.myservice.showTodayDate(); 
     this.myProperty = this.myservice.serviceProperty;
     console.log(this.myProperty);
     this.myservice.serviceProperty = "Service updated";
     console.log(this.myservice.serviceProperty);
  } 
}
