import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  title = 'Action Items pending for development';
  items = ['designing simple UI for all pages', 'create services and get dummy data from services to UI',
   'Setup backend service calls', 'Integrate backend service calls with UI', 'Implement login/Sign up mechanism',
  'Implement JWT for login or authenctication', 'Verify file upload to server', 'Verify image upload on UI', 
'Implement authentication and autherization for web service calls', 'implement image gallery on UI', 'write Unit test cases fir code coverage', 
'deploy project to server and run in PROD'];
myItem = this.items[0];

  constructor() { }

  ngOnInit(): void {
  }

}
