import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  serviceProperty = "Service Created";

  private finalData = [];
  private apiUrl = "http://jsonplaceholder.typicode.com/posts";
  private apiUrlForPhotosData = "https://jsonplaceholder.typicode.com/photos";
  private apiUrlForUsersData = "https://jsonplaceholder.typicode.com/users";

  constructor(private http: HttpClient) { }

  showTodayDate() {
    let ndate = new Date();
    return ndate;
  }

  getData() {
    return this.http.get(this.apiUrl);
  }

  getPhotosData() {
    return this.http.get(this.apiUrlForPhotosData);
  }

  getUsersData() {
    return this.http.get(this.apiUrlForUsersData);
  }

}
