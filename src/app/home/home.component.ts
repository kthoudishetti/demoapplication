import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'Kumar\'s New App 112233';
  intValue = 100;
  dateObj : number  = Date.now();
  object: Object = {foo: 'bar', baz: 'qux', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}};
  str: string = 'abcdefghij';
  // declared array of months. 
  months = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];
  isavailable = false; //variable is set to true
  myClickFUnction(event) {
    alert("Button is clicked now")
    console.log(event);
  }

  changemonths(event) {
    console.log("Changed month from the Dropdown");
    console.log(event);
    alert("Changed month from the Dropdown");
  }
  constructor() { }

  ngOnInit(): void {
  }

}
